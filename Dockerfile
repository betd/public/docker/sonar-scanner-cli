FROM alpine:latest
LABEL maintainer="Sébastien THIBAULT <sebastien.thibault@businessdecision.com>"
COPY ./sonar-scanner-cli/4.0 /opt/sonar-scanner

RUN  apk --update add --no-cache openjdk8-jre-base nodejs  ca-certificates \
	&& ln -s /opt/sonar-scanner/bin/sonar-scanner /usr/bin/sonar-scanner
RUN chmod +x /opt/sonar-scanner/bin/sonar-scanner

COPY ./sonar-cnes-report /opt/sonar-cnes-report

RUN ln -s /opt/sonar-cnes-report/bin/sonar-cnes-report /usr/bin/sonar-cnes-report
RUN chmod +x /opt/sonar-cnes-report/bin/sonar-cnes-report
